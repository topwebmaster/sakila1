<?php
require 'inc/header.php';
$name = $_POST['q'];
$actors = get_actors_by_last_name($name);
?>
<div class="content">
    <div class="mainform">
        <p>Поиск актёров по алфавиту</p>

        <form action="/index.php" method="post">
            <select name="q" id="q">
                <?php
                 echo "<option value='$name'>$name</option>";
                    $alphabet = str_split('abcdefghijklmnopqrstuvwxyz');
                    foreach($alphabet as $letter){
                        echo "<option value='$letter'>$letter</option>";
                    }
                    ?>
            </select>
            <p><button>Отправить</button></p>
        </form>

        <ul>
            <?php foreach($actors as $a){
                echo "<li>".$a->last_name."</li>";
            }
            ?>
        </ul>

    </div>
</div>
<?php require 'inc/footer.php';?>
